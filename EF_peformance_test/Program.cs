﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_peformance_test
{
    class Program
    {
        static void Main(string[] args)
        {
            using (EF_TESTEntities db = new EF_TESTEntities())
            {
                query_Random_Users(db, 80);
                //Add_Users(db, 900);
            }

            Console.WriteLine("Press enter to exit......");
            Console.ReadLine();
        }

        public static void Add_Users(EF_TESTEntities db, int amount)
        {
            User user = db.Users.LastOrDefault();
            int lastId = user == null ? 0 : user.Id;

            for (int i = 1;i <= amount; ++i)
            {
                DateTime now = DateTime.Now;
                string username = "User " + (i + lastId);
                db.Users.Add(new User
                {
                    Name = username,
                    Email = username + "@test.com",
                    CreatedTime = now,
                    UpdatedTime = now
                });
            }

            db.SaveChanges();
        }

        public static void query_Random_Users(EF_TESTEntities db, int amount)
        {
            int total = db.Users.Count();

            Console.WriteLine("Count total users in db: " + total);

            Random rand = new Random();
            var ids = new List<int>();
            for (int i = 0; i < amount; i++)
                ids.Add(rand.Next(total)); // .Next(max random number)

            test_Contains(db, 100, ids);
            test_sql(db, 100, ids);
            test_SingleOrDefault(db, 100, ids);
        }

        /*
         Testing query function
             */

        public static void test_SingleOrDefault(EF_TESTEntities db, int amount, List<int> ids)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            List<User> users = new List<User>();
            foreach(int id in ids)
            {
                User user = db.Users.SingleOrDefault(u => u.Id == id);

                if(user != null)
                    users.Add(user);
            }
            users.Sort((x, y) => { return x.Id.CompareTo(y.Id); });

            foreach (User user in users)
            {
                Console.WriteLine(user.Id.ToString()
                    + ", " + user.Name.ToString()
                    + ", " + user.Email.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    );
            }

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("test_SingleOrDefault, Runtime: " + (float)elapsedMs / 1000 + " s, users: " + users.Count);
            Console.WriteLine();
        }

        public static void test_Contains(EF_TESTEntities db, int amount, List<int> ids)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            List<User> users = db.Users.Where(u => ids.Contains(u.Id)).ToList();

            foreach (User user in users)
            {
                Console.WriteLine(user.Id.ToString()
                    + ", " + user.Name.ToString()
                    + ", " + user.Email.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    );
            }

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("test_Contains, Runtime: " + (float)elapsedMs / 1000 + " s, users: " + users.Count);
            Console.WriteLine();
        }

        public static void test_sql(EF_TESTEntities db, int amount, List<int> ids)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var values = new StringBuilder();
            values.AppendFormat("{0}", ids[0]);
            for (int i = 1; i < ids.Count; i++)
                values.AppendFormat(", {0}", ids[i]);

            var sql = string.Format(
                "SELECT * FROM [EF_TEST].[dbo].[User] WHERE [Id] IN ({0})",
                values);

            var users = db.Set<User>().SqlQuery(sql).ToList();

            foreach (User user in users)
            {
                Console.WriteLine(user.Id.ToString()
                    + ", " + user.Name.ToString()
                    + ", " + user.Email.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    + ", " + user.UpdatedTime.ToString()
                    );
            }

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("test_sql, Runtime: " + (float)elapsedMs / 1000 + " s, users: " + users.Count);
            Console.WriteLine();
        }
    }
}
